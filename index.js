const express = require("express");
const mongoose = require("mongoose")
const app = express()
const port = 3000

mongoose.connect("mongodb+srv://xavieryu:admin123@224-tarriman.lpilq7u.mongodb.net/s35-activity?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection
db.on("error",console.error.bind(console,"connection error"))
db.once("open",()=>console.log("Connected to MongoDB"))

const userSchema = new mongoose.Schema({
    username: String,
    password: String,
   
})

const User = mongoose.model("User",userSchema)
app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.post("/signup",(req,res)=>{
    User.findOne({username:req.body.username},(err,target)=>{
        if(target !== null && target.username == req.body.username){
            return res.send("User already exists")
        }else{
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            })
        
             newUser.save((saveErr,savedUser)=>{
                if(saveErr){
                    console.log(saveErr)
                }else{
                    res.status(200).send("Successfully created new user")
                }
            })
        }
    })
})
app.listen(port, () => console.log(`Server running at port ${port}`));